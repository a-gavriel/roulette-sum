const WHEEL_RADIUS = 400;
const TEXT_FONT_SIZE = 50;
var timer;
var timer_running = false;
let divisionCount = 5;  // The number of starting players.
const TOTAL_TIME = 15 * 60; // 15 minutes

// Create new wheel object specifying the parameters at creation time.
let theWheel = new Winwheel({
    'numSegments': 7,     // Specify number of segments.
    'outerRadius': WHEEL_RADIUS,   // Set outer radius so wheel fits inside the background.
    'textFontSize': TEXT_FONT_SIZE,    // Set font size as desired.
    'segments':        // Define segments including colour and text.
        [
            { fillStyle: '#9cdad7'.toString(16), text: 'Alexis', id: Math.floor(Math.random() * Date.now()) },
            { fillStyle: '#a1b6da'.toString(16), text: 'Adrian', id: Math.floor(Math.random() * Date.now()) },
            { fillStyle: '#70d5ff'.toString(16), text: 'Jafet', id: Math.floor(Math.random() * Date.now()) },
            { fillStyle: '#f2d298'.toString(16), text: 'Isaac', id: Math.floor(Math.random() * Date.now()) },
            { fillStyle: '#ebb195'.toString(16), text: 'Isai', id: Math.floor(Math.random() * Date.now()) },
            { fillStyle: '#d7a4c8'.toString(16), text: 'Isaias', id: Math.floor(Math.random() * Date.now()) },
            { fillStyle: '#9cdad7'.toString(16), text: 'Emma', id: Math.floor(Math.random() * Date.now()) }
        ].sort(() => Math.random() - 0.5),
    'animation':           // Specify the animation to use.
    {
        'type': 'spinToStop',
        'duration': 1,
        'spins': 2,
        'callbackFinished': alertPrize,
    }
});

// -------------------------------------------------------
// Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters
// note the indicated segment is passed in as a parmeter as 99% of the time you will want to know this to inform the user of their prize.
// -------------------------------------------------------
function alertPrize(indicatedSegment) {
    // Do basic alert of the segment text.
    // You would probably want to do something more interesting with this information.
    showNotification("The winner is: " + indicatedSegment.text);
    deleteName(indicatedSegment.id)
    timer = TOTAL_TIME / divisionCount;
    timer_running = true;
    resetWheel();
}


function showNotification(message) {
    // Create a div element for the notification
    var notification = document.createElement("div");
    notification.style.position = "fixed";
    notification.style.left = "50%";
    notification.style.top = "50%";
    notification.style.transform = "translate(-50%, -50%)";
    notification.style.padding = "60px"; // Increased padding
    notification.style.backgroundColor = "#f8d7da";
    notification.style.color = "#721c24";
    notification.style.border = "1px solid #f5c6cb";
    notification.style.borderRadius = "5px";
    notification.style.zIndex = "1000";
    notification.style.fontSize = "24px"; // Increased font size
    notification.textContent = message;

    // Append the notification to the body
    document.body.appendChild(notification);

    // Remove the notification after 3 seconds
    setTimeout(function() {
        document.body.removeChild(notification);
    }, 3000);
}

function startTimer(duration, display) {
    var minutes, seconds;
    timer = duration;
    setInterval(function () {
        --timer;
        if (timer < 0) {
            timer = 0;
        }

        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if ((timer == 0) && (timer_running)) {
            timer_running = false;
            showNotification("Time's up!");
        }
    }, 1000);
}


// =======================================================================================================================
// Code below for the power controls etc which is entirely optional. You can spin the wheel simply by
// calling theWheel.startAnimation();
// =======================================================================================================================
let wheelPower = 2;
let wheelSpinning = false;

// -------------------------------------------------------
// Click handler for spin button.
// -------------------------------------------------------
function startSpin() {
    // Ensure that spinning can't be clicked again while already running.
    if (wheelSpinning == false) {

        // Begin the spin animation by calling startAnimation on the wheel object.
        theWheel.startAnimation();

        // Set to true so that power can't be changed and spin button re-enabled during
        // the current animation. The user will have to reset before spinning again.
        wheelSpinning = true;
    }
}

// -------------------------------------------------------
// Function for reset button.
// -------------------------------------------------------
function resetWheel() {
    theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
    theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
    theWheel.draw();                // Call draw to render changes to the wheel.
    wheelSpinning = false;          // Reset to false to power buttons and spin can be clicked again.
}


// -------------------------------------------------------
// Name functionality.
// -------------------------------------------------------

let nameList = theWheel.segments
    .filter(segment => segment != null)
    .sort((a, b) => sortNames(a, b));

// -------------------------------------------------------
// Function for sort the list of names.
// -------------------------------------------------------
function sortNames(a, b) {
    if (a.text < b.text) {
        return -1;
    }
    if (a.text > b.text) {
        return 1;
    }
    return 0;
}

// -------------------------------------------------------
// Function for render the list of names.
// -------------------------------------------------------
function renderNames(todo) {
    localStorage.setItem('nameList', JSON.stringify(nameList));

    const list = document.querySelector('.js-name-list');
    const item = document.querySelector(`[data-key='${todo.id}']`);

    if (todo.deleted) {
        item.remove();
        if (nameList.length === 0) list.innerHTML = '';
        return
    }

    const isChecked = todo.checked ? 'done' : '';
    const node = document.createElement("li");
    node.setAttribute('class', `todo-item ${isChecked}`);
    node.setAttribute('data-key', todo.id);
    node.innerHTML = `
    <span>${todo.text}</span>
    <input class="delete-todo js-delete-todo" type="image" src="https://img.icons8.com/fluency/48/fa314a/delete-sign.png"/>
    `

    if (item) {
        list.replaceChild(node, item);
    } else {
        list.append(node);
    }
}

// -------------------------------------------------------
// Function for re-render the wheel after changes.
// -------------------------------------------------------
function renderWheel() {
    theWheel = new Winwheel({
        'numSegments': nameList.length,     // Specify number of segments.
        'outerRadius': WHEEL_RADIUS,   // Set outer radius so wheel fits inside the background.
        'textFontSize': TEXT_FONT_SIZE,    // Set font size as desired.
        'segments': nameList,
        'animation':           // Specify the animation to use.
        {
            'type': 'spinToStop',
            'duration': 1,
            'spins': 2,
            'callbackFinished': alertPrize,
        }
    });
}

// -------------------------------------------------------
// Function to add a name.
// -------------------------------------------------------
function addName(text) {
    const name = {
        text,
        id: Date.now(),
    };

    nameList.push(name);
    renderWheel();
    renderNames(name);
}

// -------------------------------------------------------
// Function to delete a name.
// -------------------------------------------------------
function deleteName(key) {
    const index = nameList.findIndex(item => item.id === Number(key));
    const name = {
        deleted: true,
        ...nameList[index]
    };
    nameList = nameList.filter(item => item.id !== Number(key));
    renderNames(name);
    renderWheel();
}

// -------------------------------------------------------
// Event listener for submiting a name from the input.
// -------------------------------------------------------
const form = document.querySelector('.js-form');
form.addEventListener('submit', event => {
    event.preventDefault();
    const input = document.querySelector('.js-name-input');

    const text = input.value.trim();
    if (text !== '') {
        addName(text);
        input.value = '';
        input.focus();
    }
});

// -------------------------------------------------------
// Event listener for deleting a name from the list.
// -------------------------------------------------------
const list = document.querySelector('.js-name-list');
list.addEventListener('click', event => {
    console.log(event.target.classList);
    if (event.target.classList.contains('js-delete-todo')) {
        const itemKey = event.target.parentElement.dataset.key;
        deleteName(itemKey);
    }
});

// -------------------------------------------------------
// Event listener for the page to load.
// -------------------------------------------------------
document.addEventListener('DOMContentLoaded', () => {
    localStorage.setItem('nameList', JSON.stringify(nameList));
    const ref = localStorage.getItem('nameList');
    if (ref) {
        nameList = JSON.parse(ref);
        nameList.forEach(t => {
            renderNames(t);
        });
    }
    
    var display = document.querySelector('#time');
    startTimer(0, display);
});

// -------------------------------------------------------
// Event listener for opening and closing the collapsible list.
// -------------------------------------------------------
var coll = document.getElementsByClassName("collapsible-button");
var i;

for (i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.display === "block") {
            content.style.display = "none";
        } else {
            content.style.display = "block";
        }
    });
}


// Function to update the label
function update_divided_time_label() {
    const divided_time = TOTAL_TIME / divisionCount;
    const mins = parseInt(divided_time / 60, 10);
    const secs = parseInt(divided_time % 60, 10).toString().padStart(2, '0');
    document.getElementById('timer-division-label').textContent = `${divisionCount} 🧍‍♂️ = ${mins}:${secs} / 🧍‍♂️`;
}

document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('add-time-button').addEventListener('click', function() {
        divisionCount += 1; // Increment the global variable
        update_divided_time_label(); // Update the label
    });

    // Initial label update
    update_divided_time_label();
});

document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('less-time-button').addEventListener('click', function() {
        divisionCount -= 1; // Increment the global variable
        update_divided_time_label(); // Update the label
    });

    // Initial label update
    update_divided_time_label();
});